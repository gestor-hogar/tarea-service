package es.jdvf.gestorhogar.tarea.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.jdvf.gestorhogar.tarea.dto.Tarea;
import es.jdvf.gestorhogar.tarea.exception.TareaException;
import es.jdvf.gestorhogar.tarea.service.TareaService;
import io.swagger.annotations.ApiOperation;

/**
 * Controller gestión tareas
 *
 */
@RestController
@RequestMapping("/api/tarea")
public class TareaController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TareaController.class);

	@Autowired
	private TareaService service;

	/**
	 * Devuelve avisos del usuario
	 * 
	 * @param email
	 * @return
	 */
	@ApiOperation(value = "Devuelve avisos del usuario.", response = Tarea.class)
	@RequestMapping(value = "/{email}/avisos", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	ResponseEntity<List<Tarea>> avisos(@PathVariable("email") String email) {
		LOGGER.info("avisos - begin - email: {}", email);
		List<Tarea> result = service.avisos(email);
		LOGGER.info("avisos - result: {}", result);
		return ResponseEntity.ok(result);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleTodoNotFound(TareaException ex) {
		LOGGER.error("Handling error with message: {}", ex.getMessage());
	}
}
