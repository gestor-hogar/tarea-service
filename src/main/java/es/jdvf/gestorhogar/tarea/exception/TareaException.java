package es.jdvf.gestorhogar.tarea.exception;

public class TareaException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TareaException(String id) {
        super(String.format("Exception with id: <%s>", id));
    }
}
