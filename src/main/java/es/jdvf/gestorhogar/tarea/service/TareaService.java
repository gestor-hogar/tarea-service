package es.jdvf.gestorhogar.tarea.service;

import java.util.List;

import es.jdvf.gestorhogar.tarea.dto.Tarea;

public interface TareaService {

	List<Tarea> avisos(String email);

}
