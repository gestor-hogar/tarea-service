package es.jdvf.gestorhogar.tarea.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Tarea {
	// Identificador
	@Id
	private String id;
	// tipo
	@NotEmpty
	private String tipo;
	// descripcion
	private String descripcion;
	// asignado
	private String asignado;
	// fecha
	private String fecha;
}
