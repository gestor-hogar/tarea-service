package es.jdvf.gestorhogar.tarea.util;

import es.jdvf.gestorhogar.tarea.dto.Tarea;
import es.jdvf.gestorhogar.tarea.entity.TareaEntity;

public class TareaUtil {

	/**
	 * Devuelve objeto DTO a partir de la entidad
	 * @param entity
	 * @return
	 */
	public static Tarea convertToDTO(TareaEntity entity) {
		return Tarea.builder().id(entity.getId())
				.tipo(entity.getTipo())
				.build();
	}
	
	/**
	 * Devuelve objeto entidad a partir del DTO
	 * @param dto
	 * @return
	 */
	public static TareaEntity convertToEntity(Tarea dto) {
		return TareaEntity.builder().id(dto.getId())
				.tipo(dto.getTipo())
				.build();
	}
}
