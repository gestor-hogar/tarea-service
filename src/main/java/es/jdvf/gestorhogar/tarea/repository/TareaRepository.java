package es.jdvf.gestorhogar.tarea.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import es.jdvf.gestorhogar.tarea.entity.TareaEntity;

public interface TareaRepository extends MongoRepository<TareaEntity, String> {

}
